import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class Article {
  int articleID;
  String name;
  String value;

  Article();
}

class Receipt {
  int receiptID;
  String issuer;
  String date;
  List<Article> articles;
  double sum;

  Receipt();
}

class Issuer {
  int issuerID;
  String name;
  String address;
  String uID;
  List<Receipt> receipts;

  Issuer();
}

class DatabaseHelper {
  final String issuerTable = 'issuer';
  final String receiptTable = 'receipt';
  final String articleTable = 'article';

  static Database _database;

  Future<Database> get database async {
    if (_database != null) {
      return _database;
    }
    _database = await initDB();
    return _database;
  }

  Future<Database> initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, 'Database');
    Database database =
        await openDatabase(path, version: 1, onCreate: onCreate);
    return database;
  }

  onCreate(Database database, int version) async {
    await database.execute(
        'CREATE TABLE $issuerTable(issuerID INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, address TEXT, UID TEXT);');
    await database.execute(
        'CREATE TABLE $receiptTable(receiptID INTEGER PRIMARY KEY AUTOINCREMENT, FK_issuerID INTEGER,  date TEXT);');
    await database.execute(
        'CREATE TABLE $articleTable(articleID INTEGER PRIMARY KEY AUTOINCREMENT, FK_receiptID INTEGER, name TEXT, value TEXT);');
    print('Tables are created!');
  }

  Future<List<Article>> getReceipt(int receiptID) async {
    var databaseConnection = await database;
    Receipt receipt = Receipt();
    receipt.articles = [];

    List<Map> list = await databaseConnection
        .rawQuery('SELECT * FROM $receiptTable WHERE receiptID = $receiptID');
    if (list.length > 0) {
      receipt.date = list[0]['date'];
    }

    list = await databaseConnection.rawQuery(
        'SELECT * FROM $articleTable WHERE FK_receiptID = $receiptID');
    for (int i = 0; i < list.length; i++) {
      Article article = Article();

      article.articleID = list[i]['articleID'];
      article.name = list[i]['name'];
      article.value = list[i]['value'];

      receipt.articles.add(article);
    }
    return receipt.articles;
  }

  Future<List<Receipt>> getAllReceipts() async {
    List<Receipt> receipts = [];
    var databaseConnection = await database;

    List<Map> list = await databaseConnection.rawQuery(
        'SELECT * FROM $receiptTable JOIN $issuerTable ON $receiptTable.FK_issuerID = $issuerTable.issuerID JOIN (SELECT FK_receiptID, ROUND(SUM(value),2) AS sum FROM $articleTable GROUP BY FK_receiptID) AS articles ON articles.FK_receiptID = $receiptTable.receiptID ORDER BY $receiptTable.receiptID DESC');
    for (int i = 0; i < list.length; i++) {
      Receipt receipt = Receipt();
      receipt.receiptID = list[i]['receiptID'];
      receipt.date = list[i]['date'];
      receipt.issuer = list[i]['name'];
      receipt.sum = list[i]['sum'];

      receipts.add(receipt);
    }

    return receipts;
  }

  Future<int> addNewIssuer(Issuer issuer) async {
    var databaseConnection = await database;

    int issuerID = issuer.issuerID;
    String issuerName = issuer.name;
    String issuerAddress = issuer.address;
    String issuerUID = issuer.uID;

    List<Map> list = await databaseConnection
        .rawQuery("SELECT * FROM $issuerTable WHERE name = '$issuerName'");
    if (list.length > 0) {
      issuerID = list[0]['issuerID'];
    } else {
      String query =
          "INSERT INTO $issuerTable(name, address, uID) VALUES('$issuerName', '$issuerAddress', '$issuerUID')";
      issuerID = await databaseConnection.transaction((transaction) async {
        return await transaction.rawInsert(query);
      });
    }

    return issuerID;
  }

  Future<int> addNewReceipt(
      int issuerID, String date, List<Article> articles) async {
    var databaseConnection = await database;

    String query =
        "INSERT INTO $receiptTable(FK_issuerID, date) VALUES('$issuerID', '$date')";
    int receiptID = await databaseConnection.transaction((transaction) async {
      return await transaction.rawInsert(query);
    });

    for (Article article in articles) {
      String name = article.name;
      String value = article.value;
      query =
          "INSERT INTO $articleTable(FK_receiptID, name, value) VALUES($receiptID, '$name', '$value')";
      await databaseConnection.transaction((transaction) async {
        return await transaction.rawInsert(query);
      });
    }

    return receiptID;
  }

  // void updateBill(Bill bill) async {
  //   var databaseConnection = await database;
  //   String query =
  //       'UPDATE $tableName SET article = \${bill.article}\', dx = \${bill.dx}\' WHERE id = ${bill.id}';
  //   await databaseConnection.transaction((transaction) async {
  //     return await transaction.rawUpdate(query);
  //   });
  // }

  void deleteReceipt(int receiptID) async {
    var databaseConnection = await database;
    String query = 'DELETE FROM $articleTable WHERE FK_receiptID = $receiptID';
    await databaseConnection.transaction((transaction) async {
      return await transaction.rawDelete(query);
    });
    query = 'DELETE FROM $receiptTable WHERE receiptID = $receiptID';
    await databaseConnection.transaction((transaction) async {
      return await transaction.rawDelete(query);
    });
  }

  Future<void> dropTableArticle() async {
    var databaseConnection = await database;
    String query = 'DROP TABLE $articleTable';
    return databaseConnection.execute(query);
  }

  Future<void> dropTableIssuer() async {
    var databaseConnection = await database;
    String query = 'DROP TABLE $issuerTable';
    return databaseConnection.execute(query);
  }

  Future<void> dropTableReceipt() async {
    var databaseConnection = await database;
    String query = 'DROP TABLE $receiptTable';
    return databaseConnection.execute(query);
  }

  Future close() async => _database.close();
}

import 'dart:io';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:text_from_image_recognizer/database_helper.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Text Recognizer'),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({this.title});

  final String title;
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final _biggerFont = const TextStyle(fontSize: 18.0);
  DatabaseHelper databaseHelper = DatabaseHelper();
  bool isImageLoaded = false;
  File image;
  String data = "";
  int id;
  String article;
  double dx;
  double dy;
  bool dialVisible = true;

  Future<List<Receipt>> future;

  @override
  void initState() {
    super.initState();
    // databaseHelper.database.then((db) {
    //   databaseHelper.onCreate(db, 1);
    // });
    // databaseHelper.initDB();
  }

  DatabaseHelper dbHelper = DatabaseHelper();
  Future getProjectDetails() async {
    List<Receipt> projetcList = await dbHelper.getAllReceipts();
    return projetcList;
  }

  Widget _buildReceiptList() {
    return FutureBuilder(
      builder: (context, projectSnap) {
        if (projectSnap.hasData != true ||
            projectSnap.connectionState == ConnectionState.none ||
            projectSnap.connectionState == ConnectionState.waiting) {
          return Container();
        }
        return ListView.builder(
          itemCount: projectSnap.data.length,
          padding: const EdgeInsets.all(0.0),
          itemBuilder: (context, index) {
            Receipt receipt = projectSnap.data[index];
            return Column(
              children: <Widget>[
                //_buildRow(receipt),
                Dismissible(
                  key: Key(receipt.date),
                  onDismissed: (direction) {
                    setState(() {
                      projectSnap.data.removeAt(index);
                      dbHelper.deleteReceipt(receipt.receiptID);
                    });
                    Scaffold.of(context).showSnackBar(
                      SnackBar(
                        content: Text("$receipt wurde gelöscht"),
                        action: SnackBarAction(
                            label: 'UNDO',
                            onPressed: () {
                              undoDelete(index, receipt);
                            }),
                      ),
                    );
                  },
                  background: deleteDataLift(),
                  secondaryBackground: deleteDataRight(),
                  child: ListTile(title: _buildRow(receipt)),
                ),
              ],
            );
          },
        );
      },
      future: getProjectDetails(),
    );
  }

  undoDelete(index, receipt) {
    setState(() {
      receipt.insert(index, receipt);
    });
  }

  Widget deleteDataLift() {
    return Container(
      alignment: Alignment.centerLeft,
      padding: EdgeInsets.only(left: 20.0),
      color: Colors.red,
      child: const Icon(
        Icons.delete,
        color: Colors.white,
        size: 35.0,
      ),
    );
  }

  Widget deleteDataRight() {
    return Container(
      alignment: Alignment.centerRight,
      padding: EdgeInsets.only(right: 20.0),
      color: Colors.red,
      child: const Icon(
        Icons.delete,
        color: Colors.white,
        size: 35.0,
      ),
    );
  }

  Future refreshReceiptList() async {
    await Future.delayed(Duration(seconds: 1));
    isImageLoaded = false;
    return _buildReceiptList();
  }

  Widget _buildRow(Receipt receipt) {
    return ListTile(
      subtitle: Text('Datum: ' + receipt.date),
      trailing: Text(receipt.sum.toString() + ' EUR'),
      leading: CircleAvatar(
        child: Text(receipt.issuer.toString().substring(0, 1).toUpperCase()),
      ),
      title: Text(
        receipt.issuer,
        style: _biggerFont,
      ),
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (BuildContext context) => SecondScreen(receipt)));
      },
    );
  }

  SpeedDial buildSpeedDial() {
    return SpeedDial(
      animatedIcon: AnimatedIcons.menu_close,
      animationSpeed: 150,
      animatedIconTheme: IconThemeData(size: 22.0),
      //child: Icon(Icons.add),
      // onOpen: () {},
      // onClose: () {},
      visible: dialVisible,
      curve: Curves.bounceIn,
      children: [
        SpeedDialChild(
          child: Icon(Icons.photo_camera, color: Colors.white),
          backgroundColor: Colors.green,
          onTap: () {
            getImageFromCamera();
          },
          labelWidget: Container(
            color: Colors.transparent,
            margin: EdgeInsets.only(right: 10),
            padding: EdgeInsets.all(6),
            child: Text(
              'Mach ein Foto!',
              style: TextStyle(fontWeight: FontWeight.w800, fontSize: 16.0),
            ),
          ),
          // label: 'Mach ein Foto!',
          // labelStyle: TextStyle(fontWeight: FontWeight.w500, fontSize: 15.0),
          // labelBackgroundColor: Colors.white,
        ),
        SpeedDialChild(
          child: Icon(Icons.photo_library, color: Colors.white),
          backgroundColor: Colors.blue,
          onTap: () {
            getImageFromGallery();
          },
          labelWidget: Container(
            color: Colors.transparent,
            margin: EdgeInsets.only(right: 10),
            padding: EdgeInsets.all(6),
            child: Text('Wähle ein Foto aus!',
                style: TextStyle(fontWeight: FontWeight.w800, fontSize: 16.0)),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: isImageLoaded
            ? Center(
                child: Column(
                  children: <Widget>[
                    Container(
                      height: 400.0,
                      width: 500.0,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                            image: FileImage(image), fit: BoxFit.contain),
                      ),
                    ),
                    SizedBox(height: 10.0),
                    RaisedButton(
                        child: Text('Read Text'),
                        onPressed: () {
                          readText();
                          setState(() {
                            _buildReceiptList();
                          });
                        }),
                    SizedBox(height: 10.0),
                    RaisedButton(
                        child: Text('Back'),
                        onPressed: () {
                          isImageLoaded = false;
                          setState(() {
                            _buildReceiptList();
                          });
                        }),
                  ],
                ),
              )
            : _buildReceiptList(),
        floatingActionButton: buildSpeedDial());
  }

  Future getImageFromGallery() async {
    var picture = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      image = picture;
      isImageLoaded = true;
    });
  }

  Future getImageFromCamera() async {
    var picture = await ImagePicker.pickImage(source: ImageSource.camera);
    setState(() {
      image = picture;
      isImageLoaded = true;
    });
  }

  Future readText() async {
    FirebaseVisionImage ourImage = FirebaseVisionImage.fromFile(image);
    TextRecognizer recognizeText = FirebaseVision.instance.textRecognizer();
    VisionText readText = await recognizeText.processImage(ourImage);

    String daRec = '';

    String s = readText.text;

    String ladenName = readText.blocks[0].lines[0].text;
    //print(ladenName);

    RegExp exp = new RegExp(
        r'(0[1-9]|[12][0-9]|3[01])[- /.]( )?(0[1-9]|1[012])[- /.]( )?(19\d\d|20\d\d|\d\d)');
    String kaufDatum = exp.stringMatch(s).replaceAll(RegExp(r"\s+\b|\b\s"), "");

    //print(kaufDatum);

    List<Article> articles = [];

    List<TextLine> allLines = [];
    for (TextBlock block in readText.blocks) {
      bool breakable = false;
      for (TextLine line in block.lines) {
        if (line.text.toLowerCase().contains("summe") ||
            line.text.toLowerCase().contains("zu zahlen") ||
            line.text.toLowerCase().contains("total")) {
          breakable = true;
          break;
        }
        allLines.add(line);
      }
      if (breakable) break;
    }

    for (TextLine leftLine in allLines) {
      TextLine nearstLine;
      double verticalOffset = double.infinity;
      bool twoColumns = false;
      for (TextLine rightLine in allLines) {
        if ((leftLine.boundingBox.topLeft.dy - rightLine.boundingBox.topLeft.dy)
                .abs() <
            0.05 * leftLine.boundingBox.topLeft.dy) {
          if (((leftLine.boundingBox.topLeft.dy -
                          rightLine.boundingBox.topLeft.dy)
                      .abs() <
                  verticalOffset) &&
              leftLine.boundingBox.topLeft.dx <
                  rightLine.boundingBox.topLeft.dx) {
            verticalOffset = (leftLine.boundingBox.topLeft.dy -
                    rightLine.boundingBox.topLeft.dy)
                .abs();
            nearstLine = rightLine;
            twoColumns = true;
          }
        }
      }

      if (twoColumns) {
        Article article = Article();
        article.name = leftLine.text;
        article.value = nearstLine.text
            .replaceAll(RegExp(r"\s\b|\b\s|A|B|€|\*"), "")
            .replaceAll(RegExp(r","), ".");

        articles.add(article);
      }

      daRec += leftLine.text + '\n';
    }
    Issuer myIssuer = Issuer();
    myIssuer.name = ladenName;
    int myIssuerID;
    Future myFuture = databaseHelper.addNewIssuer(myIssuer);
    myFuture.then((issuerID) async {
      myIssuerID = issuerID;
      await databaseHelper.addNewReceipt(myIssuerID, kaufDatum, articles);
    });

    setState(() {
      data = daRec;
    });
  }
}

class SecondScreen extends StatefulWidget {
  final receipt;
  SecondScreen(this.receipt);

  @override
  _SecondScreenState createState() => _SecondScreenState();
}

class _SecondScreenState extends State<SecondScreen> {
  final _biggerFont = const TextStyle(fontSize: 18.0);

  DatabaseHelper dbHelper = DatabaseHelper();
  Future getReceiptDetails() async {
    List<Article> articleList =
        await dbHelper.getReceipt(widget.receipt.receiptID);
    return articleList;
  }

  Widget _buildReceiptList() {
    return FutureBuilder(
        builder: (context, projectSnap) {
          if (projectSnap.hasData != true ||
              projectSnap.connectionState == ConnectionState.none ||
              projectSnap.connectionState == ConnectionState.waiting) {
            return Container();
          }
          return ListView.builder(
            itemCount: projectSnap.data.length,
            padding: const EdgeInsets.all(16.0),
            itemBuilder: (context, index) {
              Article article = projectSnap.data[index];
              return Column(children: <Widget>[
                _buildRow(article),
              ]);
            },
          );
        },
        future: getReceiptDetails());
  }

  Widget _buildRow(Article article) {
    return ListTile(
      trailing: Text(article.value),
      title: Text(
        article.name,
        style: _biggerFont,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.receipt.issuer),
      ),
      body: _buildReceiptList(),
    );
  }
}
